package com.itau.custodia.service;

import com.itau.custodia.DTO.ContratoClienteDTO;
import com.itau.custodia.DTO.ContratoInstituicaoDTO;
import com.itau.custodia.DTO.CustodiaAcessoClienteDTO;
import com.itau.custodia.DTO.CustodiaAcessoInstituicaoDTO;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustodiaService {
    public ContratoInstituicaoDTO consultaExterna(CustodiaAcessoInstituicaoDTO custodia) {
        RestTemplate consultaContratoInstituicao = new RestTemplate();
        ResponseEntity<ContratoInstituicaoDTO> exchange = consultaContratoInstituicao.exchange("http://localhost:8081/instituicao/"+custodia.getCpf()+custodia.getToken(),HttpMethod.GET,null,ContratoInstituicaoDTO.class);
        return exchange.getBody();
    }

    public void atualizaAutorizacaoCliente (CustodiaAcessoClienteDTO custodia){
        RestTemplate atualizaAutorizacao = new RestTemplate();
        ResponseEntity<ContratoClienteDTO> exchange = atualizaAutorizacao.exchange("http://localhost:8082/cliente?cpf="+custodia.getCpf()+"&senha="+custodia.getSenha()+"&consultaExterna="+custodia.getPermitirConsultaExterna(),HttpMethod.PUT,null, ContratoClienteDTO.class);
    }

    public ContratoClienteDTO consultaContratoCliente(CustodiaAcessoClienteDTO custodia){
        RestTemplate consultaContrato = new RestTemplate();
        ResponseEntity<ContratoClienteDTO> exchange = consultaContrato.exchange("http://localhost:8082/cliente?cpf="+custodia.getCpf()+"&senha="+custodia.getSenha(),HttpMethod.GET,null,ContratoClienteDTO.class);
        return exchange.getBody();
    }
}
