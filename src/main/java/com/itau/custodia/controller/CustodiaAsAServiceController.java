package com.itau.custodia.controller;

import com.itau.custodia.DTO.ContratoClienteDTO;
import com.itau.custodia.DTO.ContratoInstituicaoDTO;
import com.itau.custodia.DTO.CustodiaAcessoClienteDTO;
import com.itau.custodia.DTO.CustodiaAcessoInstituicaoDTO;
import com.itau.custodia.service.CustodiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/custodia")
public class CustodiaAsAServiceController {

    @Autowired
    private CustodiaService custodiaService;

    @RequestMapping(path = "/instituicao",method = RequestMethod.GET)
    public ContratoInstituicaoDTO custodiaAcessoInstituicao(@RequestBody CustodiaAcessoInstituicaoDTO custodia){
        return custodiaService.consultaExterna(custodia);
    }

    //cliente atualiza autorizações para consulta externa e receber notificação
    @RequestMapping(path = "/cliente",method = RequestMethod.PUT)
    public void custodiaAutorizacaoCliente(@RequestBody CustodiaAcessoClienteDTO custodia){
        custodiaService.atualizaAutorizacaoCliente(custodia);
    }

    //cliente consulta dados dos contratos que possui e configurações de autorizações de consulta externa e notificação
    @RequestMapping(path="/cliente",method = RequestMethod.GET)
    public ContratoClienteDTO custodiaConsultaCliente(@RequestBody CustodiaAcessoClienteDTO custodia){
        return custodiaService.consultaContratoCliente(custodia);
    }

}
