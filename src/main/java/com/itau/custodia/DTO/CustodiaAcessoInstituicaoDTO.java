package com.itau.custodia.DTO;

public class CustodiaAcessoInstituicaoDTO extends CustodiaAcesso {
    private String token;

    public CustodiaAcessoInstituicaoDTO(String cpf, String token) {
        super(cpf);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
