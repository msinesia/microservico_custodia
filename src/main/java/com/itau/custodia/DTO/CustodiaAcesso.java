package com.itau.custodia.DTO;

public class CustodiaAcesso {
    protected String cpf;

    public CustodiaAcesso(String cpf) {
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
