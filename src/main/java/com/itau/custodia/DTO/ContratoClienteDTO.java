package com.itau.custodia.DTO;

import java.util.Date;

public class ContratoClienteDTO extends ContratoInstituicaoDTO {
    private int numero_contrato;
    private Date dt_inicio;
    private double valor_pago;
    private char notificacao;
    private char consultaExterna;

    public ContratoClienteDTO(String status_contrato, double valor_total_contrato, double valor_aberto_contrato, Date dt_fim_contrato, int numero_contrato, Date dt_inicio, double valor_pago, char notificacao, char consultaExterna) {
        super(status_contrato,valor_total_contrato,valor_aberto_contrato,dt_fim_contrato);
        this.numero_contrato = numero_contrato;
        this.dt_inicio = dt_inicio;
        this.valor_pago = valor_pago;
        this.notificacao = notificacao;
        this.consultaExterna = consultaExterna;
    }

    public int getNumero_contrato() {
        return numero_contrato;
    }

    public void setNumero_contrato(int numero_contrato) {
        this.numero_contrato = numero_contrato;
    }

    public Date getDt_inicio() {
        return dt_inicio;
    }

    public void setDt_inicio(Date dt_inicio) {
        this.dt_inicio = dt_inicio;
    }

    public double getValor_pago() {
        return valor_pago;
    }

    public void setValor_pago(double valor_pago) {
        this.valor_pago = valor_pago;
    }

    public char getNotificacao() {
        return notificacao;
    }

    public void setNotificacao(char notificacao) {
        this.notificacao = notificacao;
    }

    public char getConsultaExterna() {
        return consultaExterna;
    }

    public void setConsultaExterna(char consultaExterna) {
        this.consultaExterna = consultaExterna;
    }
}
