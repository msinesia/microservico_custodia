package com.itau.custodia.DTO;

public class CustodiaAcessoClienteDTO extends CustodiaAcesso{
    private String senha;
    private char permitirConsultaExterna;

    public CustodiaAcessoClienteDTO(String cpf, String senha, char permitirConsultaExterna) {
        super(cpf);
        this.senha = senha;
        this.permitirConsultaExterna = permitirConsultaExterna;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public char getPermitirConsultaExterna() {
        return permitirConsultaExterna;
    }

    public void setPermitirConsultaExterna(char permitirConsultaExterna) {
        this.permitirConsultaExterna = permitirConsultaExterna;
    }
}
